var gulp        = require('gulp'); 
var browserify  = require('browserify');
var babelify    = require('babelify');
var source      = require('vinyl-source-stream');
var buffer      = require('vinyl-buffer');
var transform   = require('vinyl-transform');
var uglify      = require('gulp-uglify');
var sourcemaps  = require('gulp-sourcemaps');
var livereload  = require('gulp-livereload');
var del         = require('del');
var addsrc      = require('gulp-add-src');
var gulpif      = require('gulp-if');
const { series, parallel} = require('gulp');

var isProd = false;

var getFileName = function (filePath) {
    return filePath.replace(/.*[\/\\]/g, '');
}

var bundle = function bundle() {
    var browserified = transform(function (filePath) {
        let reg = /^\s*\/[\*\/]\s*@vendor ['"](.*)['"]/mg;
        return browserify({entries: filePath, debug: true})
        .transform("babelify", {
                presets: ["@babel/preset-env"],
                sourceMaps: true,
                sourceMapsAbsolute: true,
                plugins : [
                "@babel/plugin-proposal-class-properties",
                "@babel/plugin-transform-arrow-functions"]
        })
        .bundle()
        .pipe(source(getFileName(filePath)))
        .pipe(buffer())
        .pipe(sourcemaps.init())
        .pipe(gulpif(isProd, uglify()))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/js'))
        .pipe(livereload());
    });
    return gulp.src('./src/pages/*.js')
            .pipe(browserified);
}


exports.default =  bundle;