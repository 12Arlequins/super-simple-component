module.exports = {
    presets: [
        [
            "@babel/preset-env", {
                "spec": true,
                "loose": true,
                "targets": {
                    "browsers": ["last 2 versions", "ie >= 10"],
                    "esmodules": true
                },
                "modules": false,
                "debug": true
            }
        ]
    ],
    plugins : [
        "@babel/plugin-proposal-class-properties",
        "@babel/plugin-transform-arrow-functions"
    ]
};