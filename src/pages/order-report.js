import Component from "../components/Component";
import ColorBlinkingText from "../components/ColorBlinkingText";

Component.Register(ColorBlinkingText);
Component.Initalize();