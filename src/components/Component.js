
/**
 * @class
 * @description The component class can be extends to create a componentbundleRenderer.
 * @description Each register component will be created and the methods initalizeStates, initalizeCache, bindEvents, create will be called in this order.
 * @description Make sure to overload theses method to plug your Component
 * @example See ColorBlinkingText.js to see an example of Component and order-report.js to see a bundle file.
 */
export default class Component {
    /**
     * @static @interface
     * @member componentName
     * @description This static attribute should be defined on every new component extending Component with a unique name
     * @description such as myComponentName than this name will be truned into a snak-case name as my-component-name
     * @description than use to look for DOMNode using the selector [data-component=<snale-case-component-name>]
     * @description to initalize with this Component class.
     * @example ```static componentName = 'myComponentName'```
     */

    /**
     * @static @interface
     * @member Events
     * @description All the events used by the class 
     */
    static Events = {
        initalizeStates: 'component:initalizeStates',
        initalizeCache: 'component:initalizeCache',
        bindEvents: 'component:bindEvents',
        create: 'component:create'
    };

    /**
     * @static @private
     * @member _registeredComponents All the component classes waiting to be initalized 
     */
    static _registeredComponents = [];
    /**
     * @static @private
     * @member _componentsInstance All the instance of Component
     * */
    static _componentsInstance = [];
    /**
     * @static @public
     * @description Add Component class to the registed list.
     * @description Once the document will emit an event DOMContentLoaded all the component instance will be created
     * @description and the methods initalizeStates, initalizeCache, bindEvents, create will be called in this order.
     * @param {class} componentConstructor A class extending the Component class
     */
    static Register (componentConstructor) {
        Component._registeredComponents.push(componentConstructor);
    }
    /**
     * @static @public
     * @description Creates all the component insance base on the classes sotred into _registeredComponents.
     * @description For each class CreateComponent will look for the static memeber componentName and turn it into sanke-case.
     * @description Then he'll look for [data-component=<snale-case-component-name>] and create a compoent for each of the DOMNode matching the selecor
     * @description Onces all the instance are created event are emit on the document that triggers the methods initalizeStates, initalizeCache, bindEvents, create in this order.
     * @param {class} componentConstructor A class extending the Component class
     */
    static CreateComponent() {
        Component._registeredComponents.forEach((componentConstructor) => {
            let selector = `[data-component='${componentConstructor.componentName.split(/(?=[A-Z])/).join('-').toLowerCase()}']`
            Component._componentsInstance = [
                ...Component._componentsInstance,
                ...Array.prototype.map.call(
                    document.querySelectorAll(selector),
                    (el) => new componentConstructor(el)
                )
            ];
        })
        document.dispatchEvent(new Event(Component.Events.initalizeStates));
        document.dispatchEvent(new Event(Component.Events.initalizeCache));
        document.dispatchEvent(new Event(Component.Events.bindEvents));
        document.dispatchEvent(new Event(Component.Events.create));
    }
    /**
     * @static @public
     * @description This method has to be called by the bundle file, usualy in pages.
     * @description It will launche the function CreateComponent that will result in initalizing all the components
     * @description therfor it has to by after all the Component.Register function.
     */
    static Initalize() {
        if (document.readyState === 'interactive' || document.readyState === 'complete') {
            Component.CreateComponent();
        } else {
            document.addEventListener('DOMContentLoaded', Component.CreateComponent);
        }
    }   

    /**
     * @constructor @private
     * @description SHOULD NOT BE OVERLOAD !
     * @description Register the component to document events that will triggers the methods initalizeStates, initalizeCache, bindEvents, create in this order.
     * @param {DOMNode} domElement 
     */
    constructor (domElement) {
        this.cache = {};
        this.domElement = domElement;
        document.addEventListener(Component.Events.initalizeStates, this.initalizeStates.bind(this));
        document.addEventListener(Component.Events.initalizeCache, this.initalizeCache.bind(this));
        document.addEventListener(Component.Events.bindEvents, this.bindEvents.bind(this));
        document.addEventListener(Component.Events.create, this.create.bind(this));
    }

    /**
     * @interface @public
     * @description This method should be overload to set variables on this
     */
    initalizeStates() {
        console.log('Component:initalizeStates');
    }

    /**
     * @interface @public
     * @description This method should be overload to cache required DOMNodes
     */
    initalizeCache() {
        console.log('Component:initalizeCache');
    }

    /**
     * @interface @public
     * @description This method should be overload to bind event
     */
    bindEvents() {
        console.log('Component:bindEvents');
    }

    /**
     * @interface @public
     * @description This method should be overload to initalized you component
     */
    create() {
        console.log('Component:initalize');
    }

    /**
     * @public
     * @description Remove the component 
     */
    destroy() {
        this.removeEventListner();
        Component._componentsInstance = Component._componentsInstance.filter((i) => i !== this);
        delete this;
    }
}
