import Component from './Component';
import $ from 'jquery';
// @vendor 'selectboxit'
export default class ColorBlinkingText extends Component {
    static componentName = "ColorBlinkingText";

    initalizeStates() {
        this.colors = this.domElement.dataset.colors.split(/\s+/g);
        this.intervals = this.domElement.dataset.intervals.split(/\s+/g);
        this.colorIndex = 0;
        this.intervalIndex = 0;
        this.nextChangeFunction = null;
        this.isPaused = false;
    }

    initalizeCache() {
        this.cache.$pauseButton = $();
    }

    bindEvents() {
        this.domElement.addEventListener('click', this.pause.bind(this));
    }

    create() {
        this.changeColor();
    }

    changeColor() {
        if (!this.isPaused) {
            this.domElement.style.color = this.colors[this.colorIndex++]
            setTimeout(this.changeColor.bind(this), this.intervals[this.intervalIndex++]);
            if (this.colorIndex >= this.colors.length) {
                this.colorIndex = 0;
            }
            if (this.intervalIndex >= this.intervals.length) {
                this.intervalIndex = 0;
            }
        }
    }

    pause() {
        this.isPaused = !this.isPaused;
        if (this.isPaused) {
            this.changeColor();
        }
    }
}
