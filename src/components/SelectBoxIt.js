import Component from './Component';
// @vendor 'selectboxit'
export default class SelectBoxIt extends Component {
    static componentName = "SelectBoxIt";
    create() {
        $(this.domElement).selectBoxIt();
    }
}